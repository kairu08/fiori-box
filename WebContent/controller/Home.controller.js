sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function(Controller, History) {
	"use strict";
	return Controller.extend("sandbox.controller.Home", {
		getRouter : function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		onInit : function() {

		},

		onRouteMatched : function(oEvent) {
		},

		onAppBack : function(oEvent) {
			
		}
	});
});